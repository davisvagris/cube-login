import _ from 'lodash';
require('./bootstrap');

const registrationForm = $('form.registration-form');

let checkUsernameExists = (e) => {
    const emailInputEl = $(e.currentTarget);
    const inputHint = emailInputEl.parent();
    const inputSubmit = registrationForm.find('button[type="submit"]');
    inputHint.find('.hint').remove();

    if (emailInputEl.val().trim() === '') {
        return;
    }

    $.post(BASE_URL + '/register/check', { email: emailInputEl.val().trim() }, (response) => {
        if (!response.status) {
            inputHint.append(`<small class="text-danger hint">This user already exists!</small>`);
        }

        inputSubmit.prop('disabled', !response.status);
    });
};

if (registrationForm.length > 0) {
    registrationForm.find('input[name="email"]').keyup(_.debounce(checkUsernameExists, 250));
}