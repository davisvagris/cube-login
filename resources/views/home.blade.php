@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="card-columns">
                @foreach($news as $item)
                    <div class="card">
                        <a href="{{ $item->get('link') }}">
                            <img class="card-img-top" src="{{ $item->get('image') }}" alt="Card image cap">
                        </a>

                        <div class="card-body">
                            <a href="{{ $item->get('link') }}">
                                <h5 class="card-title">{{ $item->get('title') }}</h5>
                            </a>

                            <p class="card-text">{{ $item->get('description') }}</p>
                            <p class="card-text"><small class="text-muted">{{ $item->get('pubDate')->diffForHumans() }}</small></p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
