<?php
return [
    'cache_lifetime' => 30, // Lifetime, when fetch new data (minutes)
    'size' => 5, // How many articles to show
];