<?php

namespace App\Http\Controllers;

use App\Services\Tvnet;

class HomeController extends Controller
{
    /**
     * @var Tvnet
     */
    private $tvnet;

    /**
     * Create a new controller instance.
     *
     * @param Tvnet $tvnet
     */
    public function __construct(Tvnet $tvnet)
    {
        $this->middleware('auth');
        $this->tvnet = $tvnet;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {
        $news = $this->tvnet->get();

        return view('home', compact('news'));
    }
}
