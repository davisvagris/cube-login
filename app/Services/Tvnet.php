<?php

namespace App\Services;

use Carbon\Carbon;
use Closure;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Contracts\Cache\Repository as CacheContract;
use Illuminate\Support\Collection;
use Nathanmac\Utilities\Parser\Parser;
use Psr\Log\LoggerInterface;
use Illuminate\Contracts\Config\Repository as ConfigContract;

class Tvnet
{
    const URL = 'https://www.tvnet.lv/rss';

    /**
     * @var array
     */
    protected $httpOptions = [
        'headers' => [
            'Accept' => 'application/xml',
        ]
    ];

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Parser
     */
    protected $parser;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var CacheContract
     */
    protected $cache;

    /**
     * @var ConfigContract
     */
    protected $config;

    /**
     * Tvnet constructor.
     *
     * @param Client $client
     * @param Parser $parser
     * @param LoggerInterface $logger
     * @param CacheContract $cache
     * @param ConfigContract $config
     */
    public function __construct(Client $client, Parser $parser, LoggerInterface $logger, CacheContract $cache, ConfigContract $config)
    {
        $this->client = $client;
        $this->parser = $parser;
        $this->logger = $logger;
        $this->cache = $cache;
        $this->config = $config;
    }

    /**
     * @param string $content
     * @return \Illuminate\Support\Collection
     * @throws Exception
     */
    public function parse(string $content)
    {
        $parse = $this->parser->xml($content);
        $items = collect($parse)
            ->recursive()
            ->get('channel')
            ->get('item')
            ->map(Closure::fromCallable([$this, 'mapData']));

        return $items;
    }

    /**
     * Parse data
     * Change date to carbon function
     * Some articles don't have image, then set place holder
     *
     * @param Collection $item
     * @return Collection
     */
    protected function mapData(Collection $item)
    {
        $item['pubDate'] = Carbon::parse($item->get('pubDate'));
        $item['image'] = 'http://via.placeholder.com/480x270';

        if ($enclosure = $item->get('enclosure')) {
            $item['image'] = $enclosure->get('@url');
        }

        return $item;
    }


    /**
     * @return Collection
     * @throws Exception
     */
    protected function fetch(): Collection
    {
        $fetch = $this->client->get(self::URL, $this->httpOptions);

        if ($fetch->getStatusCode() != 200) {
            $this->logger->error("Can't get tvnet news");

            return collect([]);
        }

        return $this->parse($fetch->getBody()->getContents());
    }

    /**
     * Get config from tvnet file
     *
     * @param string $name
     * @param string $default
     * @return string
     */
    protected function getConfig(string $name, $default = '')
    {
        return $this->config->get("tvnet.{$name}", $default);
    }

    /**
     * @return \Illuminate\Support\Collection
     * @throws Exception
     */
    public function get(): Collection
    {
        return $this->cache->remember('tvnet:'.$this->getConfig('size'), $this->getConfig('cache_lifetime'), function () {
            return $this->fetch()->take($this->getConfig('size'));
        });
    }
}