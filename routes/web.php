<?php


Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware('throttle:60,1')->group(function () {
    Route::post('/register/check', 'Auth\RegisterController@checkUser');
});